console.log("hello from js");

//The first thing that we need to do is to identify which course it needs to display inside the browser.
//we are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//window.location. -> returns a location object with information about the "current" location of the document.
// .seach => contains the query string section of the current URL. //QUERY STRING IS AN OBJECT // search property returns an object of type stringString.
//URLSearchParam() -> this method/constructor creates and returns a new URLSearchParams object. (this is a class)
//"URLSearchParams" -> this describes the interface that defines utility methods to work with the query string of a URL. (this is a prop type)
// new -> instantiate a user-defined object.
// "URLSearchParams" -> template for creating the object

// params ={
	//"courseId": "....id ng course that we passed"
//}
let id = params.get('courseId')
console.log(id)

//lets capture the access token from the localstorage.
let token = localStorage.getItem('token')
console.log(token)

//lets capture the sections of the html body.
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")

fetch(`https://hidden-temple-73370.herokuapp.com/api/courses/${id}`).then(
	res => res.json()).then(data => {
		console.log(data)
		
		name.innerHTML = data.name
		desc.innerHTML = data.description
		price.innerHTML = data.price
		enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

       // addEventListener for enroll button
       //we have to capture first the anchor tag for enroll, add an event listener to trigger an event. create a function in the eventlistemer() to describe the next set of procedures
       document.querySelector('#enrollButton').addEventListener("click", () => {
       	//insert the course to our enrollment array inside the user collections.
       	fetch(`https://hidden-temple-73370.herokuapp.com/api/users/enroll`, {
       		//descrube the parts of the request.
       		method: 'POST',
       		headers: {
       			'Content-Type': 'application/json',
       			'Authorization': `Bearer ${token}` //we have to get the value of auth string. to verify and to validate the user.
       		},
       		body: JSON.stringify({ //para ma process ng ating back end
       			courseId: id //identify the value na ilalagay natin
       		})
       	})
       	.then(res => {
       		return res.json() //para maging readable siya sa ating browser
       	})
       	.then(data => { //kukunin ntn ung data na naka json format na
       		//now we can inform the user that the request has been done or not
       		if (data === true){
       			alert("Thank you for enrolling to this course.")
       			//redirect back to courses page.
       			window.location.replace("./courses.html")
       		}else{
       			//inform the user that the request has failed.
       			alert("Something went wrong.")
       		}
       	})
    	})
})
       //Group answer
       /* let enrollButton = document.querySelector("#enrollButton");
		enrollButton.addEventListener("submit", (e) => {
        e.preventDefault();
        fetch(`http://localhost:4000/api/users/enroll` , {
        	method: 'POST',
        	headers: {
        		"Content-Type": "application/json",
        		"Authorization": `Bearer ${token}`
        	},
        	body: JSON.stringify({
        		courseId: id
        	})
        }).then(res => {
        	return res.json();
        }).then(data => {
        	//console.log(data);
        if (data) {
        	Swal.fire({
        		icon: "success",
        		title: "Success",
        		text: "Course successfully enrolled",
        		confirmButtonText: "Back"
        	}).then(() => {
        		window.location.href="./courses.html"
        	})
        }	
        })
	}) */
